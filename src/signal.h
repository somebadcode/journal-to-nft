#ifndef JOURNAL_TO_NFT_SIGNAL_H
#define JOURNAL_TO_NFT_SIGNAL_H

#include <signal.h>

#include <sys/epoll.h>

typedef struct {
	struct epoll_event ev;
	int fd;
	sigset_t sigmask;
	sigset_t prev_sigmask;
} signal_t;

int signal_prepare(signal_t *signals, int epfd);
int signal_reset(signal_t *signals);
int signal_process_event(signal_t *signals);

#endif // JOURNAL_TO_NFT_SIGNAL_H
