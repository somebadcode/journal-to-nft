#include "journal.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <values.h>

#include <sys/stat.h>

int journal_prepare(journal_t *journal, char *cursor_file, char *unit, int epfd) {
	int r;

	journal->cursor_file = cursor_file;
	journal->cursor = NULL;

	r = sd_journal_open(&journal->j, SD_JOURNAL_LOCAL_ONLY);
	if (r < 0) {
		fprintf(stderr, "ERROR: sd_journal_open() returned: %s\n", strerror(-r));
		return -1;
	}

	journal->ev.events = (uint32_t) sd_journal_get_events(journal->j);
	journal->ev.data.ptr = (void *) journal;

	journal->fd = sd_journal_get_fd(journal->j);
	if (journal->fd < 0) {
		fprintf(stderr, "ERROR: sd_journal_get_fd() returned: %s\n", strerror(-r));
		sd_journal_close(journal->j);
		return -1;
	}

	if (sd_journal_reliable_fd(journal->j) == 0) {
		fputs("WARN: Unreliable file descriptor. Entries can be delayed.\n", stderr);
	}

	r = epoll_ctl(epfd, EPOLL_CTL_ADD, journal->fd, &journal->ev);
	if (r < 0) {
		fprintf(stderr, "ERROR: epoll_ctl() returned: %s\n", strerror(-r));
		close(journal->fd);
		return -1;
	}

	uint64_t t;
	r = sd_journal_get_timeout(journal->j, &t);
	if (r < 0) {
		fprintf(stderr, "ERROR: sd_journal_get_timeout() returned: %s\n", strerror(-r));
		sd_journal_close(journal->j);
		return -1;
	} else if (t == (uint64_t)(-1)) {
		journal->poll_timeout_msec = -1;
	} else {
		struct timespec ts;
		uint64_t n;
		clock_gettime(CLOCK_MONOTONIC, &ts);
		n = (uint64_t) ts.tv_sec * 1000000 + (uint64_t) ts.tv_nsec / 1000;
		journal->poll_timeout_msec = t > n ? (int) ((t - n + 999) / 1000) : 0;
	}

	if (cursor_file != NULL) {
		r = journal_load_cursor_file(journal);
		if (r < 0) {
			return -1;
		}
		r = journal_set_cursor(journal);
		if (r < 0) {
			return -1;
		}
	} else {
		sd_journal_seek_head(journal->j);
	}

	if (unit != NULL) {
		r = journal_match_unit(journal, unit);
		if (r < 0) {
			return -1;
		}
	}

	return 0;
}

void journal_close(journal_t *journal) {
	journal_save_cursor_file(journal);
	sd_journal_flush_matches(journal->j);
	sd_journal_close(journal->j);
	free(journal->cursor);
	journal->cursor = NULL;
}

int journal_set_cursor(journal_t *journal) {
	int r;

	if (journal->cursor == NULL || !strlen(journal->cursor)) {
		sd_journal_seek_head(journal->j);
		return 0;
	}

	r = sd_journal_seek_cursor(journal->j, (const char *) journal->cursor);
	if (r < 0) {
		fprintf(stderr, "ERROR: sd_journal_seek_cursor() returned: %s\n", strerror(-r));
		return -1;
	}

	sd_journal_next(journal->j);

	return 0;
}

int journal_match_unit(journal_t *journal, char *unit) {
	int r;
	char match[256 + 1] = {0};

	strncat(match, "_SYSTEMD_UNIT=", sizeof(match) - 1);
	strncat(match, unit, sizeof(match) - 1);

	r = sd_journal_add_match(journal->j, match, 0);
	if (r < 0) {
		fprintf(stderr, "ERROR: sd_journal_add_match() returned: %s\n", strerror(-r));
		return -1;
	}

	return 0;
}

int journal_process_event(journal_t *journal) {
	int r;

	r = sd_journal_process(journal->j);
	switch (r) {
		case SD_JOURNAL_NOP:
			// Nothing to do. Timeout exceeded but nothing new has arrived.
		case SD_JOURNAL_INVALIDATE:
			// Journal is being rotated or something. Nothing to do.
			break;
		case SD_JOURNAL_APPEND:
			return journal_process_entries(journal);

		default:
			fprintf(stderr, "sd_journal_process() returned (%d): %s\n", -r, strerror(-r));
			return -1;
	}
	return 0;
}

int journal_process_entries(journal_t *journal) {
	int r;
	const void *data;
	size_t len;

	while (1) {
		r = sd_journal_next(journal->j);
		if (r < 0) {
			fprintf(stderr, "sd_journal_next() returned (%d): %s\n", -r, strerror(-r));
			sd_journal_close(journal->j);
			return -1;
		} else if (r == 0) {
			break;
		}

		r = sd_journal_get_data(journal->j, "MESSAGE", (const void **) &data, &len);
		if (r < 0) {
			fprintf(stderr, "sd_journal_get_data() returned (%d): %s\n", -r, strerror(-r));
			sd_journal_close(journal->j);
			return -1;
		}
		puts((char *) data);
	}
	return 0;
}

int journal_load_cursor_file(journal_t *journal) {
	FILE *fp;
	int r;
	struct stat st;

	// Get file size.
	r = stat(journal->cursor_file, &st);
	if (errno == ENOENT) {
		journal->cursor = NULL;
		return 0;
	} else if (r != 0) {
		fprintf(stderr, "ERROR: stat() on '%s' returned: %s.\n", journal->cursor_file, strerror(errno));
		return -1;
	}

	if (st.st_size > INT_MAX) {
		fprintf(stderr, "ERROR: File '%s' is too large: %ld.\n", journal->cursor_file, st.st_size);
		return -1;
	}

	fp = fopen(journal->cursor_file, "r");
	if (fp == NULL) {
		if (errno == ENOENT) {
			return 0;
		}
		fprintf(stderr, "ERROR: fopen() on '%s' returned: %s.\n", journal->cursor_file, strerror(errno));
		return -1;
	}

	// Allocate memory
	journal->cursor = malloc((size_t) st.st_size + 1);
	if (journal->cursor == NULL) {
		fputs("ERROR: Memory allocation error.", stderr);
		fclose(fp);
		return -1;
	}

	// Read the data
	journal->cursor = fgets(journal->cursor, (int) st.st_size + 1, fp);
	r = ferror(fp);
	if (r != 0) {
		fprintf(stderr, "ERROR: fopen() of '%s' returned: %s.\n", journal->cursor_file, strerror(r));
		fclose(fp);
		free(journal->cursor);
		journal->cursor = NULL;
		return -1;
	}

	fclose(fp);
	return 0;
}

int journal_save_cursor_file(journal_t *journal) {
	FILE *fp;
	char *cursor;
	int r, ferr;

	r = sd_journal_get_cursor(journal->j, &cursor);
	if (r == -EADDRNOTAVAIL) {
		return 0;
	} else if (r < 0) {
		fprintf(stderr, "ERROR: sd_journal_get_cursor() returned: %s\n", strerror(-r));
		return -1;
	}

	fp = fopen(journal->cursor_file, "w");
	if (fp == NULL) {
		fprintf(stderr, "ERROR: fopen() on '%s' returned: %s.\n", journal->cursor_file, strerror(errno));
		free(cursor);
		cursor = NULL;
		return -1;
	}

	r = fputs(cursor, fp);
	ferr = ferror(fp);
	if (r < 0) {
		fprintf(stderr, "ERROR: fputs() on '%s' returned: %s.\n", journal->cursor_file, strerror(ferr));
		free(cursor);
		cursor = NULL;
		fclose(fp);
		return -1;
	}

	fclose(fp);
	free(cursor);
	cursor = NULL;
	return 0;
}
