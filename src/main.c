#include "hashmap.h"
#include "poll.h"

#include <argp.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

const char *argp_program_version = "POC 1.0";
const char *argp_program_bug_address = "<noreply@example.org>";
static char doc[] = "Something";
static char args_doc[] = "A B C";

enum ARGS {
	ARG_UNIT,
	ARG_NF_IPV4_TABLE,
	ARG_NF_IPV6_TABLE,
	ARG_NF_IPV4_SET,
	ARG_NF_IPV6_SET,
	ARG_VERBOSE = 'v',
	ARG_CURSOR_FILE = 'c'
};

static struct argp_option options[] = {
    {"verbose", ARG_VERBOSE, 0, 0, "Verbose output", 0},
    {"cursor-file", ARG_CURSOR_FILE, "FILE", 0, "Journal cursor file. Used to continue where the program left off.", 0},
    {"unit", ARG_UNIT, "STRING", 0, "Name of the service which journal should be watched.", 0},
    {"ipv4-table", ARG_NF_IPV4_TABLE, "TABLE", 0, "NetFilter IPv4 table.", 0},
    {"ipv6-table", ARG_NF_IPV6_TABLE, "TABLE", 0, "NetFilter IPv6 table.", 0},
    {"ipv4-set", ARG_NF_IPV4_SET, "SET", 0, "NetFilter IPv4 set.", 0},
    {"ipv6-set", ARG_NF_IPV6_SET, "SET", 0, "NetFilter IPv6 set.", 0},
    {NULL, 0, NULL, 0, NULL, 0}};

struct arguments {
	int verbose;
	char *cursor_file, *unit;
	char *nf_ipv4_table, *nf_ipv6_table;
	char *nf_ipv4_set, *nf_ipv6_set;
};

static error_t parse_opt(int key, char *arg, struct argp_state *state);

static struct argp argp = {options, parse_opt, args_doc, doc, NULL, NULL, NULL};

static error_t parse_opt(int key, char *arg, struct argp_state *state) {
	struct arguments *arguments = state->input;

	switch (key) {
		case ARG_VERBOSE:
			arguments->verbose++;
			break;
		case ARG_CURSOR_FILE:
			arguments->cursor_file = arg;
			break;

		case ARG_UNIT:
			arguments->unit = arg;
			break;

		case ARG_NF_IPV4_TABLE:
			arguments->nf_ipv4_table = arg;
			break;

		case ARG_NF_IPV6_TABLE:
			arguments->nf_ipv6_table = arg;
			break;

		case ARG_NF_IPV4_SET:
			arguments->nf_ipv4_set = arg;
			break;

		case ARG_NF_IPV6_SET:
			arguments->nf_ipv6_set = arg;
			break;

		case ARGP_KEY_END:
			break;

		default:
			return ARGP_ERR_UNKNOWN;
	}
	return 0;
}

int main(int argc, char *argv[]) {
	struct arguments arguments;

	/* Default values. */
	arguments.verbose = 0;
	arguments.cursor_file = "cursor.txt";
	arguments.unit = "sshd.service";
	arguments.nf_ipv4_table = "ingress";
	arguments.nf_ipv6_table = "ingress";
	arguments.nf_ipv4_set = "blacklist4";
	arguments.nf_ipv6_set = "blacklist6";

	argp_parse(&argp, argc, argv, 0, 0, &arguments);

	hashmap_t *map;
	int r = hashmap_create(&map, 349);
	if (r != 0) {
		fprintf(stderr, "hashmap_create failed\n");
	}

	hashmap_key_t *key = hashmap_key(map, "something", strlen("something"));
	r = hashmap_set(map, key, "what ever!", strlen("what ever!") + 1, 1);
	if (r != 0) {
		fprintf(stderr, "hashmap_set failed\n");
	}

	char *val;
	int meta = hashmap_get(map, key, (void **) &val);
	if ((val == NULL) || (meta != 1)) {
		fprintf(stderr, "hashmap_get failed: %s (%d)\n", val, meta);
	} else {
		printf("something = %s\n", val);
	}

	hashmap_unset(map, key);

	hashmap_free(map);

	printf("Verbosity = %d\nCursor file = %s\nService = %s\nIPv4 table = "
	       "%s\nIPv6 table = %s\nIPv4 set = %s\nIPv6 set = %s\n",
	       arguments.verbose, arguments.cursor_file, arguments.unit, arguments.nf_ipv4_table,
	       arguments.nf_ipv6_table, arguments.nf_ipv4_set, arguments.nf_ipv6_set);

	return poll_loop(arguments.cursor_file, arguments.unit);
}
