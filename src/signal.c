#include "signal.h"

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <sys/signalfd.h>

int signal_prepare(signal_t *signals, int epfd) {
	int r;

	sigemptyset(&signals->sigmask);
	sigaddset(&signals->sigmask, SIGTERM);
	sigaddset(&signals->sigmask, SIGINT);
	sigaddset(&signals->sigmask, SIGQUIT);

	signals->fd = signalfd(-1, &signals->sigmask, SFD_NONBLOCK);

	signals->ev.events = EPOLLIN;
	signals->ev.data.ptr = (void *) signals;

	r = sigprocmask(SIG_BLOCK, &signals->sigmask, &signals->prev_sigmask);
	if (r < 0) {
		fprintf(stderr, "ERROR: sigprocmask() returned: %s.\n", strerror(r));
		close(signals->fd);
		return -1;
	}

	r = epoll_ctl(epfd, EPOLL_CTL_ADD, signals->fd, &signals->ev);
	if (r < 0) {
		fprintf(stderr, "ERROR: epoll_ctl() returned: %s.\n", strerror(r));
		close(signals->fd);
		return -1;
	}

	return 0;
}

int signal_reset(signal_t *signals) {
	if (sigprocmask(SIG_SETMASK, &signals->prev_sigmask, NULL) < 0) {
		perror("sigprocmask");
	}

	close(signals->fd);

	return 0;
}

int signal_process_event(signal_t *signals) {
	int retn = 0;
	struct signalfd_siginfo si;

	while (1) {
		ssize_t rsz = read(signals->fd, &si, sizeof(si));
		if (rsz < 0) {
			if (errno == EAGAIN) {
				break;
			}
			perror("read");
			return -1;
		}

		if (rsz != sizeof(si)) {
			fprintf(stderr, "Something wrong\n");
			return -1;
		}

		switch (si.ssi_signo) {
			case SIGINT:
				/* Falls through. */
			case SIGTERM:
				/* Falls through. */
			case SIGQUIT:
				retn = (int) si.ssi_signo;
				break;
			default:
				retn = -1;
		}
	}
	return retn;
}
