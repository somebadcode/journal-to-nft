#ifndef JOURNAL_TO_NFT_HASHMAP_H
#define JOURNAL_TO_NFT_HASHMAP_H

#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#define XXH_INLINE_ALL
#include <xxhash.h>

typedef struct {
	unsigned char *key;
	size_t len;
	size_t index;
} hashmap_key_t;

typedef struct hashmap_entry {
	hashmap_key_t key;
	void *value;
	size_t len;
	int meta;
	struct hashmap_entry *next;
	struct hashmap_entry *prev;
} hashmap_entry_t;

typedef struct hashmap {
	size_t size;
	char *secret[XXH3_SECRET_DEFAULT_SIZE];
	hashmap_entry_t **entries;
} hashmap_t;

int hashmap_create(hashmap_t **map, size_t size);
void hashmap_free(hashmap_t *map);
hashmap_key_t *hashmap_key(hashmap_t *map, const void *key, size_t len);
int hashmap_set(hashmap_t *map, hashmap_key_t *key, const void *value, size_t len, int meta);
void hashmap_unset(hashmap_t *map, hashmap_key_t *key);
int hashmap_get(hashmap_t *map, hashmap_key_t *key, void **value);
int hashmap_set_strstr(hashmap_t *map, const char *key, const char *value, int meta);
void hashmap_unset_strstr(hashmap_t *map, const char *key);
int hashmap_get_strstr(hashmap_t *map, const char *key, char **value);

#endif // JOURNAL_TO_NFT_HASHMAP_H
