#include "poll.h"

#include "journal.h"
#include "signal.h"

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <sys/epoll.h>

int poll_loop(char *cursor_file, char *unit) {
	int r, epfd;
	signal_t signal;
	journal_t journal;
	struct epoll_event events[2];

	epfd = epoll_create(sizeof(events));
	if (epfd < 0) {
		fprintf(stderr, "ERROR: epoll_create() returned: %s\n", strerror(errno));
		return -1;
	}

	r = signal_prepare(&signal, epfd);
	if (r < 0) {
		close(epfd);
		return -1;
	}

	r = journal_prepare(&journal, cursor_file, unit, epfd);
	if (r < 0) {
		signal_reset(&signal);
		close(epfd);
		return -1;
	}

	r = journal_process_entries(&journal);
	if (r < 0) {
		r = -1;
		goto cleanup;
	}

	// Loop and poll!
	while (1) {
		int nfds = epoll_wait(epfd, events, sizeof(events), journal.poll_timeout_msec);

		if (nfds < 0) {
			fprintf(stderr, "epoll_wait() returned (%d): %s\n", errno, strerror(errno));
			r = -1;
			goto cleanup;
		}

		for (int i = 0; i < nfds; i++) {
			if (events[i].data.ptr == &signal) {
				r = signal_process_event(&signal);
				if (r < 0) {
					fprintf(stderr, "FATAL: Unhandled signal (%d)!\n", r);
					r = -1;
					goto cleanup;
				} else if (r > 0) {
					r = 0;
					goto cleanup;
				}
			} else {
				r = journal_process_event(events[i].data.ptr);
				if (r < 0) {
					r = -1;
					goto cleanup;
				}
			}
		}
	}

cleanup:
	signal_reset(&signal);
	journal_close(&journal);
	close(epfd);

	return r;
}
