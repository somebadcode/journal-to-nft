#include "hashmap.h"

#include <errno.h>
#include <string.h>
#include <strings.h>

#include <sys/random.h>

static hashmap_entry_t *hashmap_get_entry(hashmap_t *map, hashmap_key_t *key) {
	hashmap_entry_t *kv = map->entries[key->index];
	if (kv == NULL) {
		return NULL;
	}
	while (memcmp(&key->key, &kv->key, key->len) != 0) {
		if (kv->next == NULL) {
			return NULL;
		}
		kv = kv->next;
	}
	return kv;
}

hashmap_key_t *hashmap_key(hashmap_t *map, const void *key, size_t len) {
	hashmap_key_t *hkey = malloc(sizeof(hashmap_key_t));
	if (hkey == NULL) {
		return NULL;
	}

	hkey->key = malloc(len);
	if (hkey->key == NULL) {
		free(hkey);
		return NULL;
	}

	hkey->key = memcpy(hkey->key, key, len);
	hkey->len = len;

	XXH128_hash_t hash = XXH3_128bits_withSecret(key, len, map->secret, sizeof(map->secret));
	hkey->index = ((hash.high64 % map->size) + (hash.low64 % map->size)) % map->size;
	return hkey;
}

int hashmap_create(hashmap_t **map, size_t size) {
	*map = malloc(sizeof(hashmap_t));
	if (*map == NULL) {
		return errno;
	}
	(*map)->size = size;

	(*map)->entries = calloc((*map)->size, sizeof(hashmap_entry_t *));
	if ((*map)->entries == NULL) {
		free(*map);
		return errno;
	}

	ssize_t rsz = getrandom((*map)->secret, sizeof((*map)->secret), 0);
	if (rsz != sizeof((*map)->secret)) {
		return errno;
	}

	return 0;
}

void hashmap_free(hashmap_t *map) {
	/* Iterate through all root entries. */
	for (size_t n = 0; n < map->size; n++) {
		/* Skip unallocated entries. */
		if (map->entries[n] == NULL) {
			continue;
		}
		/* Iterate through linked entries and free them. */
		hashmap_entry_t *kv = map->entries[n];
		hashmap_entry_t *next;
		while (kv != NULL) {
			memset(kv->value, 0, strlen(kv->value));
			free(kv->value);

			next = kv->next;
			memset(kv, 0, sizeof(hashmap_entry_t));
			free(kv);
			kv = next;
		}
	}
	/* Final cleanup of struct. */
	memset(map->entries, 0, map->size * sizeof(hashmap_entry_t *));
	free(map->entries);
	memset(map, 0, sizeof(hashmap_t));
	free(map);
}

int hashmap_set(hashmap_t *map, hashmap_key_t *key, const void *value, size_t len, int meta) {
	void *new_value = malloc(len);
	if (new_value == NULL) {
		return errno;
	}
	memcpy(new_value, value, len);

	hashmap_entry_t **kv = &map->entries[key->index];
	if (*kv == NULL) {
		*kv = malloc(sizeof(hashmap_entry_t));
		if (*kv == NULL) {
			return errno;
		}
		(*kv)->prev = NULL;
		(*kv)->next = NULL;
	} else {
		while (key->len != (*kv)->key.len && memcmp((*kv)->key.key, (*key).key, (*key).len) != 0) {
			if ((*kv)->next == NULL) {
				(*kv)->next = malloc(sizeof(hashmap_entry_t));
				if ((*kv)->next == NULL) {
					return errno;
				}
				(*kv)->next->prev = *kv;
				*kv = (*kv)->next;
				(*kv)->next = NULL;
				break;
			}
			*kv = (*kv)->next;
		}
	}

	(*kv)->key = *key;
	(*kv)->value = new_value;
	(*kv)->meta = meta;
	return 0;
}

void hashmap_unset(hashmap_t *map, hashmap_key_t *key) {
	hashmap_entry_t *kv;

	if (map == NULL) {
		return;
	}

	kv = hashmap_get_entry(map, key);
	if (kv == NULL) {
		return;
	}

	if (kv->prev != NULL) {
		kv->prev->next = kv->next;
	}

	/* Clear out entry and free it. */
	free(kv->value);
	memset(kv, 0, sizeof(hashmap_entry_t));
	free(kv);

	/* Remove reference to entry from array if the entry is at the root. */
	if (kv == map->entries[key->index]) {
		map->entries[key->index] = NULL;
	}
}

int hashmap_get(hashmap_t *map, hashmap_key_t *key, void **value) {
	hashmap_entry_t *kv = hashmap_get_entry(map, key);
	if (kv == NULL) {
		*value = NULL;
		return 0;
	}
	*value = kv->value;
	return kv->meta;
}

int hashmap_set_strstr(hashmap_t *map, const char *key, const char *value, int meta) {
	hashmap_key_t *k = hashmap_key(map, (const void*)key, strlen(key));
	return hashmap_set(map, k, (const void*)value, strlen(value) + 1, meta);
}

void hashmap_unset_strstr(hashmap_t *map, const char *key) {
	hashmap_key_t *k = hashmap_key(map, (const void*)key, strlen(key));
	hashmap_unset(map, k);
}

int hashmap_get_strstr(hashmap_t *map, const char *key, char **value) {
	hashmap_key_t *k = hashmap_key(map, (const void*)key, strlen(key));
	return hashmap_get(map, k, (void**)value);
}
