#ifndef JOURNAL_TO_NFT_JOURNAL_H
#define JOURNAL_TO_NFT_JOURNAL_H

#include <sys/epoll.h>

#include <systemd/sd-journal.h>

typedef struct {
	sd_journal *j;
	int fd;
	char *cursor;
	char *cursor_file;
	int poll_timeout_msec;
	struct epoll_event ev;
} journal_t;

int journal_prepare(journal_t *journal, char *cursor_file, char *unit, int epfd);
void journal_close(journal_t *journal);
int journal_set_cursor(journal_t *journal);
int journal_match_unit(journal_t *journal, char *unit);
int journal_process_event(journal_t *journal);
int journal_process_entries(journal_t *journal);
int journal_load_cursor_file(journal_t *journal);
int journal_save_cursor_file(journal_t *journal);

#endif // JOURNAL_TO_NFT_JOURNAL_H
